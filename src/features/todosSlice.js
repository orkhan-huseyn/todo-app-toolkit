import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export const getTodos = createAsyncThunk(
  'todos/fetchAllTodos',
  async function () {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos');
    const todos = await response.json();
    return todos;
  }
);

const initialState = {
  todos: [],
  loading: false,
  error: null,
};

const todosSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    addTodo(state, action) {
      const newTodo = action.payload;
      state.todos.push(newTodo);
    },
    removeTodo(state, action) {
      const id = action.payload;
      const index = state.todos.findIndex((t) => t.id === id);
      state.todos.splice(index, 1);
    },
    markTodoAsCompleted(state, action) {
      const { id, checked } = action.payload;
      const todo = state.todos.find((t) => t.id === id);
      todo.completed = checked;
    },
  },
  extraReducers: {
    [getTodos.pending]: (state) => {
      state.loading = true;
    },
    [getTodos.fulfilled]: (state, action) => {
      state.loading = false;
      state.todos = action.payload;
    },
    [getTodos.rejected]: (state, action) => {
      state.loading = false;
      state.error = 'Shit happens!';
    },
  },
});

export const { addTodo, removeTodo, markTodoAsCompleted } = todosSlice.actions;
export default todosSlice.reducer;
