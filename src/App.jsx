import uniqid from 'uniqid';
import { useDispatch, useSelector } from 'react-redux';
import {
  addTodo,
  getTodos,
  markTodoAsCompleted,
  removeTodo,
} from './features/todosSlice';
import { useEffect } from 'react';

function App() {
  const dispatch = useDispatch();
  const { todos, loading, error } = useSelector((state) => state.todos);

  useEffect(() => {
    dispatch(getTodos());
  }, []);

  function handleFormSubmit(event) {
    event.preventDefault();
    const { todoText } = event.target.elements;
    const newTodo = {
      id: uniqid(),
      title: todoText.value,
      completed: false,
    };
    dispatch(addTodo(newTodo));
    todoText.value = '';
  }

  function handleChangeCompleted(id, checked) {
    dispatch(
      markTodoAsCompleted({
        id,
        checked,
      })
    );
  }

  function handleDelete(id) {
    dispatch(removeTodo(id));
  }

  return (
    <div className="container">
      <h1 className="text-center">Todo App</h1>
      <form autoComplete="off" onSubmit={handleFormSubmit}>
        <div className="mb-3">
          <label htmlFor="todoText" className="form-label">
            Todo text
          </label>
          <input
            type="text"
            className="form-control"
            id="todoText"
            placeholder="name@example.com"
          />
        </div>
      </form>
      <ul className="list-group">
        {loading && <h1 className="text-center">Loading...</h1>}
        {todos.map((todo) => (
          <li key={todo.id} className="list-group-item">
            <div className="d-flex justify-content-between">
              <input
                defaultChecked={todo.completed}
                onChange={(e) =>
                  handleChangeCompleted(todo.id, e.target.checked)
                }
                type="checkbox"
              />
              <span className="d-block ms-2 me-auto">
                {todo.completed ? <del>{todo.title}</del> : todo.title}
              </span>
              <button
                onClick={() => handleDelete(todo.id)}
                className="btn btn-sm btn-danger"
              >
                Delete
              </button>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default App;
